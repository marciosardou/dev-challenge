export const openCart = () => {
  const cartElement = document.querySelector('.cart-box')

  if(cartElement.style.opacity === "1") {
    cartElement.style.opacity = "0";
    cartElement.style.visibility = "hidden"
    cartElement.style.transition = "linear 0.7s"
  } else {
    cartElement.style.opacity = "1"
    cartElement.style.visibility = "visible"
    cartElement.style.transition = "linear 0.7s"
  }
}