import React from "react";
import { Provider } from 'react-redux';

import store from './features/store'
import Header from "./components/Header/index";

import Collection from "./pages/Collection";
import Intern from "./pages/Intern";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <Router>
      <Provider store={store}>
        <Header />
      <div className="page-content">
        <Switch>
          <Route exact path="/" component={Collection} />
          <Route exact path="/:productName" component={Intern} />
        </Switch>
      </div>
      </Provider>
    </Router>
  );
}

export default App;
