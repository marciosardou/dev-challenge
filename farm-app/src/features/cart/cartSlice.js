import { createSlice } from '@reduxjs/toolkit'


export const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    clothInfo: [],
    clothQuantity: 0,
  },

  reducers: {
    addCloth: (state, { payload }) => {
      state.clothQuantity += 1
      state.clothInfo.push(payload)

    },
    removeCloth: (state) => {
      state.clothQuantity -= 1
    },
  },
})

export const { addCloth, removeCloth } = cartSlice.actions

export default cartSlice.reducer