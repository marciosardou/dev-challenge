import { createSlice } from '@reduxjs/toolkit'




export const search = createSlice({
  name: 'search',
  initialState: {
    value : ''
  },

  reducers: {
    sendClothData: (state, {payload}) => {
      return {
        ...state,
        value: payload
      }
    }
  },
})

export const { sendClothData } = search.actions

export default search.reducer