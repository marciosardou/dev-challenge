import { configureStore } from "@reduxjs/toolkit";

import cart from './cart/cartSlice'
import searchCloth from './searchCloth/searchSlice'



const store = configureStore({
    reducer: {
      cart,
      search: searchCloth,
    }
})

export default store;