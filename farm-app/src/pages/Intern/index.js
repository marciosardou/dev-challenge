import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";

import { addCloth, removeCloth } from '../../features/cart/cartSlice'
import api from "../../services/api";

function Intern() {
  const [clothData, setClothData] = useState();

  const location = useLocation();
  const dispatch = useDispatch()


  const handlePrice = (cloth) => {
    const priceToShow = cloth.items[0].sellers[0].commertialOffer.ListPrice;

    return priceToShow;
  };


  useEffect(() => {
    async function getClothData() {
      const response = await api.get("/");
      const clothInfo = response.data.filter(
        (cloth) => cloth.productId === location.state
      );
      setClothData(clothInfo[0]);
    }
    getClothData();
  }, []);

  return (
    <React.Fragment>
      {!clothData ? (
        ""
      ) : (
        <>
          <div className="margin"></div>
          <div className="intern-container">
            <section className="images-wrapper">
              {clothData?.items.map((cloth, i) => (
                <img
                  src={cloth.images[i].imageUrl}
                  alt={cloth.images[i].imageText}
                  key={cloth.images[i].imageId}
                />
              ))}
            </section>

            <section className="infos-wrapper">
              <span className="info-ref">Ref {clothData.productReference}</span>
              <p className="info-title">{clothData.productTitle}</p>
              <div className="price-content">
              <span>{`R$ ${handlePrice(clothData).toLocaleString("pt-br", {
                style: "currency",
                currency: "BRL",
              })}`}</span>
              <span>{`ou 10x de ${(handlePrice(clothData) / 10).toLocaleString(
                "pt-br",
                { style: "currency", currency: "BRL" }
              )}`}</span>

              </div>

              <div className="heigth-content">
                <label>Tamanho </label>
                {clothData?.items.map((cloth) => (
                  <div className="height-box" key={cloth.itemId}>
                    {cloth.Tamanho}
                  </div>
                ))}
              </div>

              <button onClick={() => dispatch(addCloth(clothData)) }>Adicionar a Sacola</button>
            </section>
          </div>
        </>
      )}
    </React.Fragment>
  );
}

export default Intern;
