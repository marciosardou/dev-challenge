import React, {useEffect} from 'react';

import Banner from '../../components/Banner'
import ShopList from '../../components/ShopList'

import { openCart } from '../../utils/index'

import './index.scss';

function Collection() {

  useEffect(() => {
    const cartElement = document.querySelector('.cart-box')
      cartElement.style.opacity = "0";
      cartElement.style.transition = "linear 0.7s"
  }, []);

  
  return (
    <>
      <Banner />
      <ShopList />
    </>
  );
}

export default Collection;