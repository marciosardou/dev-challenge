import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";

import { openCart } from '../../utils/index'


import "./index.scss";

function CartCpnt() {
  const [total, setTotal] = useState([]);

  const clothInfo = useSelector((state) => state.cart.clothInfo);

  const handlePrice = (cloth) => {
    const priceToShow = cloth.items?.[0].sellers[0].commertialOffer.ListPrice;

    return priceToShow?.toLocaleString("pt-br", {
      style: "currency",
      currency: "BRL",
    });
  };

  useEffect(() => {
    const priceOfEachCloth = clothInfo.map(
      (cloth) => cloth.items[0].sellers[0].commertialOffer.ListPrice
    );

    setTotal(
      priceOfEachCloth.reduce(
        (total, numero) => parseInt(total) + parseInt(numero),
        0
      )
    );
  }, [clothInfo]);

  return (
    <React.Fragment>
      <div className="cart-box">
        {clothInfo?.map((cloth) => (
          <div className="cloth-box" key={cloth.productId}>
            <div className="img-name-box">
              <img src={cloth.items?.[0].images[0].imageUrl} alt="cloth" />
              <span>{cloth.productName}</span>
            </div>
            <span>{handlePrice(cloth)}</span>
          </div>
        ))}

        <div className="finish-box">
          <div>
            <span className="total-text">Total :</span>
            <span className="total-price">
              {total.toLocaleString("pt-br", {
                style: "currency",
                currency: "BRL",
              })}
            </span>
          </div>
          <button>Finalizar Compra</button>
        </div>
      </div>
    </React.Fragment>
  );
}

export default CartCpnt;
