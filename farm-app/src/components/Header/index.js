import React, {useState} from 'react';
import { useSelector,useDispatch } from 'react-redux'

import Cart from '../CartCpnt'
import { openCart} from '../../utils/index'

import imgCart from '../../assets/cartImg.png'
import imgFind from '../../assets/busca.svg'


import { sendClothData } from '../../features/searchCloth/searchSlice';
import './index.scss';

function Header() {

const [inptValue, setInptValue] = useState('')


const cartQuantity = useSelector((state) => state.cart.clothQuantity);
const dispatch = useDispatch()


const handleChange = (event) => {
  setInptValue(event.target.value);
};

const handleSearch = (inptValue) => {
 dispatch(sendClothData(inptValue))
 localStorage.setItem('searchValue', inptValue)
}

  return (
    <header>
      <p>ANIMALE</p>
      <nav>
        <ul>
          <li>NOVIDADES</li>
          <li>COLEÇÃO</li>
          <li>JÓIAS</li>
          <li>SALE</li>
          <li>IMAGEM</li>
        </ul>

          <div className="utility">
              {/* <input type="text"  value={inptValue} onChange={handleChange}/> */}
              <div onClick={() => handleSearch(inptValue)}>
                <img src={imgFind} alt="imagem da busca"/>
              </div>
            <div onClick={() => openCart()}>
            <img src={imgCart} alt="imagem do carrinho"/>
            <span>{cartQuantity}</span>
            </div>
            <Cart />
          </div>
      </nav>
    </header>
  );
}

export default Header;