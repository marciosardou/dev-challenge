import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import Filter from "../Filter/index";

import api from "../../services/api";

import "./index.scss";

function ShopList() {
  const [clothes, setClothes] = useState([]);
  const [hasCloth, setHasCloth] = useState(false);


  const imagePath = (cloth) => {
    const newImagePath = cloth.items[0].images[0].imageUrl;

    return newImagePath;
  };

  const handlePrice = (cloth) => {
    const priceToShow = cloth.items[0].sellers[0].commertialOffer.ListPrice;

    return priceToShow;
  };

  useEffect(() => {
    async function requestData() {
      const response = await api.get("/")
      setClothes([...clothes, response.data]);
    }
    requestData();
    setHasCloth(true);
  }, []);

  return !hasCloth ? (
    ""
  ) : (
    <>
      <Filter />
      
      <div className="cloths-wrapper">
        {clothes[0]?.map((cloth) => (
          <div className="cloth-container" key={cloth.productId}>
            <div className="img-wrapper">
              <img src={imagePath(cloth)} alt="Imagem da roupa" />
            </div>
            <div className="info-wrapper">
              <p>{cloth.productTitle}</p>
              <div className="price">
                <span className="price-first">{`R$ ${handlePrice(cloth).toLocaleString("pt-br", {
                  style: "currency",
                  currency: "BRL",
                })}`}</span>
                <span>{` ou 10x de ${(handlePrice(cloth) / 10).toLocaleString(
                  "pt-br",
                  { style: "currency", currency: "BRL" }
                )}`}</span>
              </div>
              <Link
                to={{
                  pathname: `/${cloth.productTitle}`,
                  search: `?productId=${cloth.productId}`,
                  state: cloth.productId,
                }}
              >
                <button>QUICK SHOP</button>
              </Link>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}

export default ShopList;
